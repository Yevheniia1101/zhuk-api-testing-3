import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const users = new UsersController();
const auth = new AuthController();

describe(`Users tests.`, () => {
    let accessToken: string;
    let userId: number;
        
    it(`1-Registration was successful`, async () => {
        let response = await users.createNewUser("string", 'mytest24@test.com', 'jenya4', 'test');
        userId = response.body.user.id;  
        console.log('User ID is ', userId);   
           
       expect(response.statusCode, 'Status Code should be 201').to.be.equal(201)
       expect(response.timings.phases.total, 'Response time should be less than 4 seconds').to.be.lessThan(400);
       expect(response.body.user.userName, 'User Name is').to.be.equal('jenya4');
       console.log('Status Code for registration is ', response.statusCode)
       console.log('Time for registration is ', response.timings.phases.total)
       console.log('User Name is ', response.body.user.userName)
        }); 
       

          it(`2 - Get list of all users`, async () => {
            let response = await users.getAllUsers();
    
            expect(response.body.length, `Response body should have more than 1 item`).to.be.greaterThan(1); 
            expect(response.statusCode, 'Status Code should be 200').to.be.equal(200)
            expect(response.timings.phases.total, 'Response time should be less than 7 seconds').to.be.lessThan(700);
            console.log('Status Code for get list of all users is ', response.statusCode)
            console.log('Time for get list of all users is ', response.timings.phases.total)
            console.log('Response body consists of ', response.body.length, 'items')
            
            });  

            
            
            it(`3 - Login, get the token, get user id`, async () => {
                let response = await auth.login("mytest24@test.com", "test");
                userId = response.body.user.id;
                accessToken = response.body.token.accessToken.token;
                
                expect(response.statusCode, 'Status Code should be 200').to.be.equal(200)
                expect(response.timings.phases.total, 'Response time should be less than 5 seconds').to.be.lessThan(500);
                
                console.log('Status Code for login is ', response.statusCode)
                console.log('Time for login is ', response.timings.phases.total)
                console.log('User ID', userId);
                console.log('User Name is ', response.body.user.userName) 
                console.log('token', accessToken);
                
            });  
        
            it(`4 - Get the data of the currect user`, async () => {
                let response = await users.getCurrentUser(accessToken);
                expect(response.statusCode, 'Status Code should be 200').to.be.equal(200);
                expect(response.timings.phases.total, 'Response time should be less than 5 seconds').to.be.lessThan(500);
                
                console.log('Status Code for display info for current user is ', response.statusCode)
                console.log('Time for for display info for current user is ', response.timings.phases.total)
                console.log('User info', response.body);
            });
        
           
            it(`5 - Update User`, async () => {
                let userData: object = {
                    id: userId,
                    avatar: "string",
                    email: "mytest24@test.com",
                    userName: "jenya12",
                };
        
                let response = await users.updateUser(userData, accessToken);
                expect(response.statusCode, `Status Code should be 204`).to.be.equal(204);
                expect(response.timings.phases.total, 'Response time should be less than 5 seconds').to.be.lessThan(500);
                expect(response.body.userName) === 'jenya12';

                console.log('Status Code for update user info is ', response.statusCode);
                console.log('Time for for update user info is ', response.timings.phases.total);
                               
            });
        
            it (`6 - Get User's new info by Token`, async () => {
                let response = await users.getCurrentUser(accessToken);
                
                expect(response.statusCode, 'Status Code should be 200').to.be.equal(200);
                expect(response.body.userName, 'New User Name is').to.be.equal('jenya12');
                expect(response.timings.phases.total, 'Response time should be less than 5 seconds').to.be.lessThan(500);
                
                console.log('User new info by Token', response.body);
                console.log('Status Code for get user info by token is ', response.statusCode);
                console.log('Time for update user info by token is ', response.timings.phases.total);
            });

        
            it (`7 - Get User's info by ID`, async () => {
                let response = await users.getUserById(userId);
                
                expect(response.statusCode, 'Status Code should be 200').to.be.equal(200);
                expect(response.body.userName, 'User Name is').to.be.equal('jenya12');
                expect(response.timings.phases.total, 'Response time should be less than 5 seconds').to.be.lessThan(500);
                
                console.log('User info by ID', response.body);
                console.log('Status Code for get User info by ID is ', response.statusCode);
                console.log('Time for Get User info by ID is ', response.timings.phases.total);
                console.log('Get User info by ID: User Name', response.body.userName);
            });
        
        
            it (`8 - Delete current user by ID`, async () => {
                let response = await users.deleteUserById(userId, accessToken);
                expect(response.statusCode, 'Status Code should be 204').to.be.equal(204);
                expect(response.timings.phases.total, 'Response time should be less than 5 seconds').to.be.lessThan(500);
                
                console.log('Delete Status Code', response.statusCode);
                console.log('Time for delete User by ID is ', response.timings.phases.total);
            });

       })  


       
import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const users = new UsersController();
const auth = new AuthController();
const chai = require('chai');
chai.use(require('chai-json-schema'));

describe(`Negative tests, Login, Update User with invalid data, Get User info by invalid ID`, () => {
    let accessToken: string;
    let userId: number;
     

    before (`Registration`, async () => {
       
        let response = await users.createNewUser("string", 'mytest20@test.com', 'jenya4', 'test');
         userId = response.body.user.id;  
          }); 

        it(`1 NEGATIVE - Login with invalid email`, async () => {
            let response = await auth.login("mtest8@test.com", "test");
            expect(response.statusCode, 'Status Code should be 404').to.be.equal(404)
            expect(response.timings.phases.total, 'Response time should be less than 5 seconds').to.be.lessThan(500);
            console.log('Status Code for invalid login is ', response.statusCode)
            console.log('Time for invalid login is ', response.timings.phases.total)

        });

         it (`Login to the system`, async () => {
            let response = await auth.login("mytest20@test.com", "test");
            userId = response.body.user.id;
            accessToken = response.body.token.accessToken.token;
            console.log('User ID', userId);
        });
    
        it(`2 NEGATIVE - Update User with invalid data`, async () => {
            let userData: object = {
                id: 100000000000,
                avatar: "string",
                email: "mytest20@test.com",
                userName: "jenya22",
            };
    
            let response = await users.updateUser(userData, accessToken);
            expect(response.statusCode, `Status Code should be 400`).to.be.equal(400);
            expect(response.timings.phases.total, 'Response time should be less than 5 seconds').to.be.lessThan(500);
            console.log('Status Code for invalid update is ', response.statusCode)
            console.log('Time for invalid update is ', response.timings.phases.total)

        }); 

        it(`3 NEGATIVE - Get the data of the currect user by invalid token`, async () => {
            let response = await users.getCurrentUser('bla-bla-bla');
            expect(response.statusCode, 'Status Code should be 401').to.be.equal(401);
            expect(response.timings.phases.total, 'Response time should be less than 5 seconds').to.be.lessThan(500);
            console.log('Status Code for invalid token is ', response.statusCode)
            console.log('Time for invalid token is ', response.timings.phases.total)
        });

        it (`Delete current user by ID`, async () => {
            let response = await users.deleteUserById(userId, accessToken);
             }); 
    
    });

    
        
    

   
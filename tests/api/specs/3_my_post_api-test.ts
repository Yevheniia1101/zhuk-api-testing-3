import { expect } from "chai";
import { PostsController } from "../lib/controllers/posts.controller";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const users = new UsersController();
const auth = new AuthController();
const posts = new PostsController();
const chai = require('chai');
chai.use(require('chai-json-schema'));

describe(`Post tests. Add new Post`, () => {
    
    let accessToken: string;
    let userId: number;
    let postId: number;
    let commentId: number;

    before (`Registration and Login to the system`, async () => {
        let response = await users.createNewUser("string", 'mytest32@test.com', 'jenya4', 'test');
       
        let response_login = await auth.login("mytest32@test.com", "test");
        userId = response_login.body.user.id;
        accessToken = response_login.body.token.accessToken.token;
        console.log('User ID', userId); 
         });

        it (`1-Add a new post`, async () => {
            let postData: object = {
                id: userId,
                avatar: "string",
                previewImage: "string",
                body: "My new post"
            };

            let response = await posts.createNewPost(postData, accessToken);
            postId = response.body.id;
            expect(response.statusCode, 'Status Code should be 200').to.be.equal(200)
            expect(response.timings.phases.total, 'Response time should be less than 5 seconds').to.be.lessThan(500);
            expect(response.body.body, 'New Post is').to.be.equal('My new post');

            console.log('Status Code for add new post is ', response.statusCode)
            console.log('Time for add new post is ', response.timings.phases.total)
            console.log('post ID', postId);
        });  

            it (`2-Display all posts`, async () => {
            let response = await posts.getAllPosts();
            expect(response.statusCode, 'Status Code should be 200').to.be.equal(200);
            expect(response.timings.phases.total, 'Response time should be less than 10 seconds').to.be.lessThan(1000);
            expect(response.body.length, `Response body should have more than 1 item`).to.be.greaterThan(1);

            console.log('Status Code for display all posts is ', response.statusCode)
            console.log('Time for display all posts is ', response.timings.phases.total)
            console.log('Response body consists of ', response.body.length, 'items')
        });  

         it (`3-Add Like to my Post`, async () => {
         let likeData: object = {
                entityId: postId,
                isLike: true,
                id: userId};

            let response = await posts.addLikeReaction(likeData, accessToken);
            expect(response.statusCode, 'Status Code should be 200').to.be.equal(200);
            expect(response.timings.phases.total, 'Response time should be less than 7 seconds').to.be.lessThan(700);
            
            console.log('Status Code for add Like to Post is ', response.statusCode);
            console.log('Time for add Like to Post is ', response.timings.phases.total)
            
         });

       
        it (`4-Add a comment to my post by ID`, async () => {
            let commentData: object = {
                authorId: userId,
                postId: postId,
                body: 'The first comment to this Post'};
            let response = await posts.addCommentToPost(commentData, accessToken);
            commentId = response.body.id;          
            
            expect(response.statusCode, 'Status Code should be 200').to.be.equal(200);
            expect(response.timings.phases.total, 'Response time should be less than 10 seconds').to.be.lessThan(1000);
            
            console.log('my comment ID is', commentId); 
            console.log('Status Code for add a comment to Post is ', response.statusCode);
            console.log('Time for add a comment to Post is ', response.timings.phases.total)

            });

            it (`Delete current user by ID`, async () => {
                let response = await users.deleteUserById(userId, accessToken);
                
            });

        
})   